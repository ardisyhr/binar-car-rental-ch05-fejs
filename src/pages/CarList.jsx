import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import CarCard from "../components/CarCard";
import Reload from "../components/Reload";
import { getCarList } from "../store/actions/carAction";


const CarList = () => {
    const { carList, isLoading } = useSelector((state) => state.carReducer); 
    const dispatch = useDispatch();   

    const handleCarList = () => {
        return (
            <>
                {!isLoading ? carList?.map((car) => {
                    return (
                        <div>
                            <CarCard data={car} />
                        </div>)
                })
                    : <Reload />
                }
            </>
        )
    }
    useEffect(() => {
        document.title = "Hasil Pencarian";
        getCarList(dispatch)
    }, [dispatch]);
    return (
        <div className="w-full px-16 justify-center flex mb-16 mt-52 xl:mt-0">
            <div className="grid grid-cols-1 md:grid-cols-2 xl:grid-cols-3 gap-6">
                {handleCarList()}
            </div>
        </div>
    )

}

export default CarList;