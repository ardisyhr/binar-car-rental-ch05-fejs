import axios from "axios";
import { carTypes } from "../actiontypes/carTypes";

export const getCarList = async (dispatch) => {
    dispatch({ type: carTypes.loading, payload: true });
    return await axios
        (process.env.REACT_APP_BASE_API_URL + "/admin/car")
        .then(res => {
            dispatch({
                type: carTypes.list,
                payload: res.data
            })
            .then(dispatch({ type: carTypes.loading, payload: false }))
        })
        
}

export const getCarById = async (dispatch, id) => {
    dispatch({ type: carTypes.loading, payload: true });
    return await axios
        (process.env.REACT_APP_BASE_API_URL + "/admin/car/" + id)
        .then(res => {
            dispatch({
                type: carTypes.selection,
                payload: res.data
            })
            .then(dispatch({ type: carTypes.loading, payload: false }))
        })
} 