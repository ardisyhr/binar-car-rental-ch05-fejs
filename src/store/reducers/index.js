import carReducer from "./carReducer";
import { combineReducers } from "redux";

const rootReducer = combineReducers({ carReducer });

export default rootReducer;