import { carTypes } from "../actiontypes/carTypes";

const initialState = {
    carList: [],
    carSelection: {},
    carSpecs: {
        capacity: 4,
        transmission: "Manual",
        productionYear: 2020
    },
    isLoading: false
};
const carReducer = (state = initialState, action) => {
    switch (action.type) {
        case carTypes.list:
            return {
                ...state,
                carList: action.payload
            };
        case carTypes.selection:
            return {
                ...state,
                carSelection: action.payload
            };
        case carTypes.loading:
            return {
                ...state,
                isLoading: action.payload
            };
        default: return { ...state };
    }
}

export default carReducer;