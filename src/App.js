import './App.css';
import React from 'react';
import Navbar from './components/Navbar'
import Footer from "./components/Footer";
import Home from './components/Home';
import { Outlet } from "react-router-dom";
import Search from './components/Search';
import "./styles/tailwind.css"
import { useEffect } from 'react';

function App() {
  useEffect(() => {
    document.title = "Home"
  },[])
  return (
    <>
      <Navbar />
      <Home />
      <Search />
      <Outlet />
      <Footer />
    </>
  );
}

export default App;
